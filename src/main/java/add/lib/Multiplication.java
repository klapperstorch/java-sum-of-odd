package add.lib;

import add.even.EvenNumber;
import add.even.EvenNumberVisitor;
import add.even.EvenSuccessor;
import add.even.Zero;
import add.odd.OddNumber;

import static add.lib.Addition.add;

public class Multiplication {

    public static EvenNumber multiply(EvenNumber a, OddNumber b) {
        return add(multiply(a, b.predecessor()), a);
    }

    public static EvenNumber multiply(OddNumber a, EvenNumber b) {
        return b.accept(new EvenNumberVisitor<EvenNumber>() {

            @Override
            public EvenNumber visitZero() {
                return new Zero();
            }

            @Override
            public EvenNumber visitSuccessor(EvenSuccessor bb) {
                return add(multiply(a, bb.predecessor()), a);
            }
        });
    }

    public static OddNumber multiply(OddNumber a, OddNumber b) {
        return add(multiply(a, b.predecessor()), a);
    }

    public static EvenNumber multiply(EvenNumber a, EvenNumber b) {
        return b.accept(new EvenNumberVisitor<EvenNumber>() {

            @Override
            public EvenNumber visitZero() {
                return new Zero();
            }

            @Override
            public EvenNumber visitSuccessor(EvenSuccessor bb) {
                return add(multiply(a, bb.predecessor()), a);
            }
        });
    }
}
