package add.lib;

import add.even.EvenNumber;
import add.even.EvenNumberVisitor;
import add.even.EvenSuccessor;
import add.odd.OddNumber;

public class Addition {

    public static OddNumber add(EvenNumber a, OddNumber b) {
        return add(a.successor(), b.predecessor());
    }

    public static OddNumber add(OddNumber a, EvenNumber b) {
        return b.accept(new EvenNumberVisitor<OddNumber>() {

            @Override
            public OddNumber visitZero() {
                return a;
            }

            @Override
            public OddNumber visitSuccessor(EvenSuccessor bb) {
                return add(a.successor(), bb.predecessor());
            }
        });
    }

    public static EvenNumber add(OddNumber a, OddNumber b) {
        return add(a.successor(), b.predecessor());
    }

    public static EvenNumber add(EvenNumber a, EvenNumber b) {
        return b.accept(new EvenNumberVisitor<EvenNumber>() {

            @Override
            public EvenNumber visitZero() {
                return a;
            }

            @Override
            public EvenNumber visitSuccessor(EvenSuccessor bb) {
                return add(a.successor(), bb.predecessor());
            }
        });
    }
}
